package com.example.aswin.a6_menuandsettings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tvContent;
    TextView tvOptionSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvContent = findViewById(R.id.tvContent);
        tvOptionSetting = findViewById(R.id.tvOptionSetting);

        registerForContextMenu(tvContent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean option = sharedPreferences.getBoolean("option_setting", false);
        tvOptionSetting.setText(String.format("Option Setting: %b", option));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option_menu_about:
                Toast.makeText(this, "About selected!", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.option_menu_setting:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
//                Toast.makeText(this, "Setting selected!", Toast.LENGTH_SHORT).show();
                return true;
            default :
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = new MenuInflater(this);

        if(v == tvContent) {
            inflater.inflate(R.menu.context_menu, menu);
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu_change_color:
                tvContent.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                return true;
            case R.id.context_menu_change_text:
                tvContent.setText("Content Updated!");
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void showPopup(View view) {
        PopupMenu popupMenu = new PopupMenu(this, tvContent);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.context_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.context_menu_change_color:
                        tvContent.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                        return true;
                    case R.id.context_menu_change_text:
                        tvContent.setText("Content Updated!");
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }
}
